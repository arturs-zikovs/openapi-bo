//
//  BOATransactionsVC.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOATransactionsPresenter.h"
#import "BOATransactionsView.h"

@interface BOATransactionsVC: UIViewController <BOATransactionsView>

@property (nonatomic, strong) BOATransactionsPresenter *presenter;

@end

