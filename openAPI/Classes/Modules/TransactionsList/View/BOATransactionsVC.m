//
//  BOATransactionsVC.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOATransactionsVC.h"

@interface BOATransactionsVC () <UITableViewDelegate, UITableViewDataSource>

//IBOutlets
@property (weak, nonatomic) IBOutlet UITableView *transactionsTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dataLoadIndicator;

@end

@implementation BOATransactionsVC

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.presenter updateView];
}

#pragma mark - TableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter.interactor getTransactionsListCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.presenter prepareReusableCellForTableView:tableView
                                                                  withIndex:indexPath.row];
    return cell;
}

#pragma mark - Accounts view delegate

- (void)updateTableAfterDataLoad {
    [self.transactionsTableView reloadData];
    
    //Quick and ugly solution =( don't judge too hard
    self.dataLoadIndicator.hidden = YES;
    self.transactionsTableView.hidden = NO;
}

@end

