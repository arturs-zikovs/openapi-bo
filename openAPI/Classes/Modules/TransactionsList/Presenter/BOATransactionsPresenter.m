//
//  BOATransactionsPresenter.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOATransactionsPresenter.h"

#define accountCellIdentifier @"transactionsCell"

@interface BOATransactionsPresenter()

@end

@implementation BOATransactionsPresenter

- (void)updateView {
    [self.interactor downloadTransactionsList];
}

- (UITableViewCell *)prepareReusableCellForTableView: (UITableView *)tableView withIndex: (NSInteger)index {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:accountCellIdentifier];
    
    if (cell == nil) {
        
        //Should be changed to custom cell if implemented properly
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:accountCellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
    }
    
    cell.textLabel.text = [self.interactor getTransactionAmountByIndex:index];
    cell.detailTextLabel.text = [self.interactor getTransactionDetailsByIndex:index];
    
    return cell;
}

#pragma mark - Interactor output

- (void)updateTransactions:(NSArray *)transactionsList {
    [self.view updateTableAfterDataLoad];
}

@end
