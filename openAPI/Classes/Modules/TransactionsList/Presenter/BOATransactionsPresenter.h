//
//  BOATransactionsPresenter.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOATransactionsView.h"
#import "BOATransactionsInteractorIO.h"

@interface BOATransactionsPresenter: NSObject <BOATransactionsInteractorOutput>
@property (nonatomic, weak)     id<BOATransactionsView>            view;
@property (nonatomic, strong)   id<BOATransactionsInteractorInput> interactor;

- (void)updateView;
- (UITableViewCell *)prepareReusableCellForTableView: (UITableView *)tableView withIndex: (NSInteger)index;
@end

