//
//  BOATransactionsInteractor.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOATransactionsInteractor.h"
#import "BOANetworkService.h"

@interface BOATransactionsInteractor()

//Variables
@property (nonatomic, strong) NSArray *transactionsList;
@property (nonatomic) NSString *accountID;
@end

@implementation BOATransactionsInteractor

#pragma mark - Account list management

- (void)downloadTransactionsList {
    
    DataDownloadBlock downloadBlock = ^(NSDictionary *downloadedData) {
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            
            //Load downloaded data
            self.transactionsList = downloadedData[@"transactions"][@"booked"];
            [self.output updateTransactions:self.transactionsList];
        });
    };
    
    //Building date for request
    NSDateComponents *minusYear = [NSDateComponents new];
    minusYear.year = -1;
    NSDate *startDate = [[NSCalendar currentCalendar] dateByAddingComponents:minusYear
                                                                      toDate:[NSDate date]
                                                                     options:0];
    
    [NETWORKING requestTransationsListForAccountWithID:self.accountID
                                         withStartDate:startDate
                                               endDate:[NSDate date]
                                    andCompletionBlock:downloadBlock];
}

- (NSInteger)getTransactionsListCount {
    return [self.transactionsList count];
}

- (NSString *)getTransactionAmountByIndex:(NSInteger)transactionIndex {
    NSDictionary *transactionObject = self.transactionsList[transactionIndex];
    
    NSString *transactionAmount = [NSString stringWithFormat:@"%@ %@",
                                   transactionObject[@"amount"][@"content"],
                                   transactionObject[@"amount"][@"currency"]];
    return transactionAmount;
}

- (NSString *)getTransactionDetailsByIndex:(NSInteger)transactionIndex {
    NSDictionary *transactionObject = self.transactionsList[transactionIndex];
    
    NSString *debitorOrCreditor = ((transactionObject[@"debtor"] != nil) ? transactionObject[@"debtor"] : transactionObject[@"creditor"]);
    
    return [NSString stringWithFormat:@"Booked on %@ by %@",
            transactionObject[@"booking_date"],
            debitorOrCreditor];
}

#pragma mark - Account ID management

- (void)provideAccountID: (NSString *)accountID {
    self.accountID = accountID;
}

- (NSString *)retrieveAccountID {
    return self.accountID;
}

@end
