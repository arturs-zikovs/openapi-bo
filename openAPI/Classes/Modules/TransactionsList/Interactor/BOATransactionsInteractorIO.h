//
//  BOATransactionsInteractorIO.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BOATransactionsInteractorInput <NSObject>

- (void)downloadTransactionsList;

- (void)provideAccountID: (NSString *)accountID;
- (NSString *)retrieveAccountID;

- (NSInteger)getTransactionsListCount;
- (NSString *)getTransactionAmountByIndex: (NSInteger)transactionIndex;
- (NSString *)getTransactionDetailsByIndex: (NSInteger)transactionIndex;
@end

@protocol BOATransactionsInteractorOutput <NSObject>
- (void)updateTransactions:(NSArray *)transactionsList;
@end

