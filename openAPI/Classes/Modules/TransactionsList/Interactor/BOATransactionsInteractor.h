//
//  BOATransactionsInteractor.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BOATransactionsInteractorIO.h"

@interface BOATransactionsInteractor: NSObject <BOATransactionsInteractorInput>
@property (nonatomic, weak) id<BOATransactionsInteractorOutput> output;
@end

