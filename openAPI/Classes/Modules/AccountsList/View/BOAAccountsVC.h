//
//  BOAAccountsVC.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountsPresenter.h"
#import "BOAAccountsView.h"

@interface BOAAccountsVC: UIViewController <BOAAccountsView>

@property (nonatomic, strong) BOAAccountsPresenter *presenter;

@end

