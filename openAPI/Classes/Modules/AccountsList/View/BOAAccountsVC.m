//
//  BOAAccountsVC.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountsVC.h"

@interface BOAAccountsVC () <UITableViewDelegate, UITableViewDataSource>

//IBOutlets
@property (weak, nonatomic) IBOutlet UITableView *accountsTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *dataLoadIndicator;

@end

@implementation BOAAccountsVC

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.presenter updateView];
}

#pragma mark - TableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter.interactor getAccountsListCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *extractedExpr = [self.presenter prepareReusableCellForTableView:tableView
                                                                           withIndex:indexPath.row];
    UITableViewCell *cell = extractedExpr;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.presenter displayBalancesViewForRowAtIndex:indexPath.row];
}

#pragma mark - Accounts view delegate

- (void)updateTableAfterDataLoad {
    [self.accountsTableView reloadData];
    
    //Quick and ugly solution =( don't judge too hard
    self.dataLoadIndicator.hidden = YES;
    self.accountsTableView.hidden = NO;
}

- (void)displayViewController: (UIViewController *)viewController  {
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
