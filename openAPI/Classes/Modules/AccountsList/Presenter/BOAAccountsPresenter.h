//
//  BOAAccountsPresenter.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountsView.h"
#import "BOAAccountsInteractorIO.h"

@interface BOAAccountsPresenter: NSObject <BOAAccountsInteractorOutput>
@property (nonatomic, weak)     id<BOAAccountsView>            view;
@property (nonatomic, strong)   id<BOAAccountsInteractorInput> interactor;

- (void)updateView;
- (void)displayBalancesViewForRowAtIndex: (NSInteger)index;
- (UITableViewCell *)prepareReusableCellForTableView: (UITableView *)tableView withIndex: (NSInteger)index;
@end

