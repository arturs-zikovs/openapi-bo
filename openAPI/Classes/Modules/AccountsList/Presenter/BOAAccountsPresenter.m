//
//  BOAAccountsPresenter.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountsPresenter.h"
#import "BOAAccountBalancesVC.h"
#import "BOAAccountBalancesPresenter.h"
#import "BOAAccountBalancesInteractor.h"

#define accountCellIdentifier @"accountCell"

@interface BOAAccountsPresenter()

@end

@implementation BOAAccountsPresenter

- (void)updateView {
    [self.interactor downloadAccountsList];
}

- (void)displayBalancesViewForRowAtIndex: (NSInteger)index {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BOAAccountBalancesVC *balancesVC = [storyboard instantiateViewControllerWithIdentifier:@"balancesVC"];
    BOAAccountBalancesPresenter *presenter = [[BOAAccountBalancesPresenter alloc] init];
    BOAAccountBalancesInteractor *interactor = [[BOAAccountBalancesInteractor alloc] init];
    
    balancesVC.presenter = presenter;
    presenter.view = balancesVC;
    
    presenter.interactor = interactor;
    interactor.output = presenter;
    
    //Setting account information
    [balancesVC.presenter.interactor provideAccountID:[self.interactor getAccountIDByIndex:index]];
    [self.view displayViewController:balancesVC];
}

- (UITableViewCell *)prepareReusableCellForTableView: (UITableView *)tableView withIndex: (NSInteger)index {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:accountCellIdentifier];
    
    if (cell == nil) {
        
        //Should be changed to custom cell if implemented properly
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:accountCellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    cell.textLabel.text = [self.interactor getAccountNameByIndex:index];
    
    return cell;
}

#pragma mark - Interactor output

- (void)updateAccounts:(NSArray *)accountsList {
    [self.view updateTableAfterDataLoad];
}

@end
