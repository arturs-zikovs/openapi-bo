//
//  BOAAccountsInteractorIO.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BOAAccountsInteractorInput <NSObject>
- (void)downloadAccountsList;

- (NSInteger)getAccountsListCount;
- (NSString *)getAccountIDByIndex: (NSInteger)accountIndex;
- (NSString *)getAccountNameByIndex: (NSInteger)accountIndex;
@end

@protocol BOAAccountsInteractorOutput <NSObject>
- (void)updateAccounts:(NSArray *)accountsList;
@end

