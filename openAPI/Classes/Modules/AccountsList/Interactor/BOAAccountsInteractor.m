//
//  BOAAccountsInteractor.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountsInteractor.h"
#import "BOANetworkService.h"

@interface BOAAccountsInteractor()
@property (nonatomic, strong) NSArray *accountsList;
@end

@implementation BOAAccountsInteractor

#pragma mark - Account list management

- (void)downloadAccountsList {
    
    DataDownloadBlock accountsDownloadBlock = ^(NSDictionary *downloadedData) {
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            self.accountsList = downloadedData[@"account_list"];
            [self.output updateAccounts:self.accountsList];
        });
    };
    
    [NETWORKING requestAvailableAccountsWithBalanceInfo:NO
                                    withCompletionBlock:accountsDownloadBlock];
}

- (NSInteger)getAccountsListCount {
    return [self.accountsList count];
}

- (NSString *)getAccountIDByIndex:(NSInteger)accountIndex {
    NSDictionary *accountObject = self.accountsList[accountIndex];
    return accountObject[@"id"];
}

- (NSString *)getAccountNameByIndex:(NSInteger)accountIndex {
    NSDictionary *accountObject = self.accountsList[accountIndex];
    return [NSString stringWithFormat:@"%@(%@)", accountObject[@"name"], accountObject[@"currency"]];
}

@end
