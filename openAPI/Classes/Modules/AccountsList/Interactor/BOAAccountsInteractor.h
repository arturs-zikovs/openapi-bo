//
//  BOAAccountsInteractor.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountsInteractorIO.h"

@interface BOAAccountsInteractor: NSObject <BOAAccountsInteractorInput>
@property (nonatomic, weak) id<BOAAccountsInteractorOutput> output;
@end

