//
//  BOALoginPresenter.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOALoginView.h"

@interface BOALoginPresenter: NSObject
@property (nonatomic, weak)     id<BOALoginView>   view;

- (void)showLoginError;
- (void)prepareForTransitionWithSegue:(UIStoryboardSegue *)segue;
@end

