//
//  BOALoginPresenter.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOALoginPresenter.h"
#import "BOAAccountsVC.h"
#import "BOAAccountsPresenter.h"
#import "BOAAccountsInteractor.h"

@interface BOALoginPresenter ()

@end

@implementation BOALoginPresenter

- (void)showLoginError {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Log In Failed"
                                                                   message:@"Log In is not available at the moment, you can skip in for now."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                
                                }];
    
    [alert addAction:okButton];
    [self.view displayViewController: alert];
}

- (void)prepareForTransitionWithSegue:(UIStoryboardSegue *)segue {
    //Initializing objects
    BOAAccountsVC *accountsVC = segue.destinationViewController.childViewControllers[0];
    BOAAccountsPresenter *presenter = [[BOAAccountsPresenter alloc] init];
    BOAAccountsInteractor *interactor = [[BOAAccountsInteractor alloc] init];
    
    accountsVC.presenter = presenter;
    presenter.view = accountsVC;
    
    presenter.interactor = interactor;
    interactor.output = presenter;
}

@end
