//
//  BOALoginVC.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOALoginVC.h"

@interface BOALoginVC ()

@end

@implementation BOALoginVC

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.presenter = [[BOALoginPresenter alloc] init];
    self.presenter.view = self;
}

#pragma mark - Button handling

- (IBAction)loginButtonTapped:(id)sender {
    [self.presenter showLoginError];
}

#pragma mark - Transition management

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self.presenter prepareForTransitionWithSegue:segue];
}

#pragma mark - Accounts view delegate

- (void)displayViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

@end
