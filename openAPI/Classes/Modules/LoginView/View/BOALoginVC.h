//
//  BOALoginVC.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOALoginPresenter.h"
#import "BOALoginView.h"

@interface BOALoginVC : UIViewController <BOALoginView>

@property (nonatomic, strong) BOALoginPresenter *presenter;
@end

