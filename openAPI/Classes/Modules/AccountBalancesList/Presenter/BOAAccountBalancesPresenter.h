//
//  BOAAccountBalancesPresenter.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountBalancesView.h"
#import "BOAAccountBalancesInteractorIO.h"

@interface BOAAccountBalancesPresenter: NSObject <BOAAccountBalancesInteractorOutput>
@property (nonatomic, weak)     id<BOAAccountBalancesView>            view;
@property (nonatomic, strong)   id<BOAAccountBalancesInteractorInput> interactor;

- (void)updateView;
- (void)displayTransactionsView;
@end

