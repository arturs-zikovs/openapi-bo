//
//  BOAAccountBalancesPresenter.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountBalancesPresenter.h"
#import "BOATransactionsVC.h"
#import "BOATransactionsPresenter.h"
#import "BOATransactionsInteractor.h"

@interface BOAAccountBalancesPresenter()

@end

@implementation BOAAccountBalancesPresenter

- (void)updateView {
    [self.interactor downloadAccountInfo];
}

- (void)displayTransactionsView {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BOATransactionsVC *transactionsVC = [storyboard instantiateViewControllerWithIdentifier:@"transactionsVC"];
    BOATransactionsPresenter *presenter = [[BOATransactionsPresenter alloc] init];
    BOATransactionsInteractor *interactor = [[BOATransactionsInteractor alloc] init];
    
    transactionsVC.presenter = presenter;
    presenter.view = transactionsVC;
    
    presenter.interactor = interactor;
    interactor.output = presenter;
    
    //Setting account information
    [transactionsVC.presenter.interactor provideAccountID:[self.interactor retrieveAccountID]];
    [self.view displayViewController:transactionsVC];
}

#pragma mark - Interactor output

- (void)updateAccountInfo {
    [self.view updateLabelsAfterDataLoad];
}

@end
