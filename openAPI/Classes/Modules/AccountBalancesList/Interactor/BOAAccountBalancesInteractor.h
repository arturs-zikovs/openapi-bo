//
//  BOAAccountBalancesInteractor.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountBalancesInteractorIO.h"

@interface BOAAccountBalancesInteractor: NSObject <BOAAccountBalancesInteractorInput>
@property (nonatomic, weak) id<BOAAccountBalancesInteractorOutput> output;

@end

