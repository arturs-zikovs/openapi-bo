//
//  BOAAccountBalancesInteractorIO.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BOAAccountBalancesInteractorInput <NSObject>

- (void)downloadAccountInfo;

- (void)provideAccountID: (NSString *)accountID;
- (NSString *)retrieveAccountID;

- (NSString *)getAccountDataByKey: (NSString *)key;
- (NSString *)getExpectedBalanceData;
- (NSString *)getBookedBalanceData;
- (NSString *)getAvailableBalanceData;

@end

@protocol BOAAccountBalancesInteractorOutput <NSObject>
- (void)updateAccountInfo;
@end

