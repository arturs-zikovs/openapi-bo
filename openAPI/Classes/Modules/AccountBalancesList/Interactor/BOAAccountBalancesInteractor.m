//
//  BOAAccountBalancesInteractor.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountBalancesInteractor.h"
#import "BOANetworkService.h"

@interface BOAAccountBalancesInteractor ()

//Variables
@property (nonatomic) NSDictionary *accountInfo;
@property (nonatomic) NSString *accountID;

@end

@implementation BOAAccountBalancesInteractor

#pragma mark - Balances management

- (void)downloadAccountInfo {
    
    DataDownloadBlock downloadBlock = ^(NSDictionary *downloadedData) {
        
        dispatch_async(dispatch_get_main_queue(), ^() {
            
            self.accountInfo = downloadedData;
            
            //Filling UI with downloaded data
            [self.output updateAccountInfo];
        });
    };
    
    [NETWORKING requestAccountInformationForAccountWithID:self.accountID
                                          withBalanceInfo:YES
                                       andCompletionBlock:downloadBlock];
}

- (NSString *)getAccountDataByKey: (NSString *)key {
    return self.accountInfo[key];
}

#pragma mark - Balances

//Quick solution to save some time... not safe at all
- (NSString *)getExpectedBalanceData {
    NSDictionary *balancesData = self.accountInfo[@"balances"];
    return [NSString stringWithFormat:@"%@ %@",
            balancesData[@"expected"][@"amount"][@"content"],
            balancesData[@"expected"][@"amount"][@"currency"]];
}
- (NSString *)getBookedBalanceData {
    NSDictionary *balancesData = self.accountInfo[@"balances"];
    return [NSString stringWithFormat:@"%@ %@",
            balancesData[@"booked"][@"amount"][@"content"],
            balancesData[@"booked"][@"amount"][@"currency"]];
}
- (NSString *)getAvailableBalanceData {
    NSDictionary *balancesData = self.accountInfo[@"balances"];
    return [NSString stringWithFormat:@"%@ %@",
            balancesData[@"interim_available"][@"amount"][@"content"],
            balancesData[@"interim_available"][@"amount"][@"currency"]];
}

#pragma mark - Account ID management

- (void)provideAccountID: (NSString *)accountID {
    self.accountID = accountID;
}

- (NSString *)retrieveAccountID {
    return self.accountID;
}

@end
