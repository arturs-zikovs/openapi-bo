//
//  BOAAccountBalancesView.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BOAAccountBalancesView <NSObject>
- (void)updateLabelsAfterDataLoad;
- (void)displayViewController: (UIViewController *)viewController;
@end
