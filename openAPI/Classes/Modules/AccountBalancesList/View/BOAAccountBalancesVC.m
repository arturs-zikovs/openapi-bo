//
//  BOAAccountBalancesPresenter.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOAAccountBalancesVC.h"

@interface BOAAccountBalancesVC ()

//IBOutlets
@property (weak, nonatomic) IBOutlet UILabel *accountNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *IBANLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountCurrencyLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookedBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *expectedBalanceLabel;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

@end

@implementation BOAAccountBalancesVC

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.presenter updateView];
}

#pragma mark - Button handling

- (IBAction)transactionButtonTapped:(id)sender {
    
    [self.presenter displayTransactionsView];
}

#pragma mark - Account balances view delegate

- (void)updateLabelsAfterDataLoad {
    
    //Filling UI with downloaded data
    self.accountNameLabel.text       = [self.presenter.interactor getAccountDataByKey:@"name"];
    self.IBANLabel.text              = [self.presenter.interactor getAccountDataByKey:@"iban"];
    self.accountCurrencyLabel.text   = [self.presenter.interactor getAccountDataByKey:@"currency"];
    
    self.bookedBalanceLabel.text = [self.presenter.interactor getBookedBalanceData];
    self.availableBalanceLabel.text = [self.presenter.interactor getAvailableBalanceData];
    self.expectedBalanceLabel.text = [self.presenter.interactor getExpectedBalanceData];
    
    self.loadingView.hidden = YES;
}

- (void)displayViewController: (UIViewController *)viewController  {
    [self.navigationController pushViewController:viewController animated:YES];
}

@end



