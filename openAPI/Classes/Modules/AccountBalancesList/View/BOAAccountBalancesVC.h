//
//  BOAAccountBalancesVC.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BOAAccountBalancesPresenter.h"
#import "BOAAccountBalancesView.h"

@interface BOAAccountBalancesVC: UIViewController <BOAAccountBalancesView>

@property (nonatomic, strong) BOAAccountBalancesPresenter *presenter;

@end

