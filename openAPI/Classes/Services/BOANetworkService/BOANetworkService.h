//
//  BOANetworkService.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#define NETWORKING     [BOANetworkService sharedNetworkService]

#import "BOANetworkServiceDefines.h"


@interface BOANetworkService : NSObject {
    
}

#pragma mark - Shared instance
+ (BOANetworkService *)sharedNetworkService;

#pragma mark - Network request management

- (void)requestAvailableAccountsWithBalanceInfo:(BOOL)shouldReturnBalanceInfo
                            withCompletionBlock:(DataDownloadBlock)completionBlock;

- (void)requestAccountInformationForAccountWithID:(NSString *)accountID
                                  withBalanceInfo:(BOOL)shouldReturnBalanceInfo
                               andCompletionBlock:(DataDownloadBlock)completionBlock;

- (void)requestTransationsListForAccountWithID:(NSString *)accountID
                                 withStartDate:(NSDate *)startDate
                                       endDate:(NSDate *)endDate
                            andCompletionBlock:(DataDownloadBlock)completionBlock;

@end
