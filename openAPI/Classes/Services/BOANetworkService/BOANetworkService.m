//
//  BOANetworkService.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//


#import "BOANetworkService.h"
#import "BOANetworkService+Helpers.h"

static BOANetworkService *_staticNetworkService = nil;

@interface BOANetworkService () <NSURLSessionDelegate>

//Variables

@property (nonatomic, strong) NSString *sessionID;

@end

@implementation BOANetworkService

#pragma mark - Initialization
- (id)init {
    self = [super init];
    if (self) {
        // Initial initialization of common objects
        
        self.sessionID = nil;

    }
    return self;
}

#pragma mark - Shared instance
+ (BOANetworkService *)sharedNetworkService {
    static dispatch_once_t sharedNetworkTag = 0;
    
    dispatch_once(&sharedNetworkTag, ^{
        _staticNetworkService = [[BOANetworkService alloc] init];
    });
    
    return _staticNetworkService;
}

#pragma mark - Data request operations

- (void)requestAvailableAccountsWithBalanceInfo:(BOOL)shouldReturnBalanceInfo
                            withCompletionBlock:(DataDownloadBlock)completionBlock {
    
    NSDictionary *requestParams = @{@"with-balance": (shouldReturnBalanceInfo)? @"true" : @"false"};

    [self performGETRequestWithURL:kEndpointAccountsURL
                    requestHeaders:nil
                 requestParameters:requestParams
                andCompletionBlock:completionBlock];
}

- (void)requestAccountInformationForAccountWithID:(NSString *)accountID
                                  withBalanceInfo:(BOOL)shouldReturnBalanceInfo
                               andCompletionBlock:(DataDownloadBlock)completionBlock {
    
    NSString *URLString = [NSString stringWithFormat:kEndpointAccountByIDURL, accountID];
    NSDictionary *requestParams = @{@"with-balance": (shouldReturnBalanceInfo)? @"true" : @"false"};
    
    [self performGETRequestWithURL:URLString
                    requestHeaders:nil
                 requestParameters:requestParams
                andCompletionBlock:completionBlock];
    
}

- (void)requestTransationsListForAccountWithID:(NSString *)accountID
                                 withStartDate:(NSDate *)startDate
                                       endDate:(NSDate *)endDate
                            andCompletionBlock:(DataDownloadBlock)completionBlock {
    
    NSString *URLString = [NSString stringWithFormat:kEndpointTransactionsURL, accountID];
    
    
    NSMutableDictionary *requestParams = [[NSDictionary dictionary] mutableCopy];
    [requestParams setValue:(startDate) ? [self convertDateToISODate:startDate] : nil forKey:@"date_from"];
    [requestParams setValue:(endDate) ? [self convertDateToISODate:endDate] : nil forKey:@"date_to"];
    
    [self performGETRequestWithURL:URLString
                    requestHeaders:nil
                 requestParameters:nil
                andCompletionBlock:completionBlock];
}

#pragma mark - NSURLSession delegate methods

-(void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
    

    // Get remote certificate
    SecTrustRef serverTrust = challenge.protectionSpace.serverTrust;
        SecCertificateRef certificate = SecTrustGetCertificateAtIndex(serverTrust, 0);
    
    // Set SSL policies for domain name check
    NSMutableArray *policies = [NSMutableArray array];
    [policies addObject:(__bridge_transfer id)SecPolicyCreateSSL(true, (__bridge CFStringRef)challenge.protectionSpace.host)];
    SecTrustSetPolicies(serverTrust, (__bridge CFArrayRef)policies);
    
    // Evaluate server certificate
    SecTrustResultType result;
    SecTrustEvaluate(serverTrust, &result);
    BOOL certificateIsValid = (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified);
    
    // Get local and remote cert data
    NSData *remoteCertificateData = CFBridgingRelease(SecCertificateCopyData(certificate));
    NSString *pathToCert = [[NSBundle mainBundle] pathForResource:@"certificate" ofType:@"cer"];
    NSData *localCertificate = [NSData dataWithContentsOfFile:pathToCert];
    
    // The pinning check
    if ([remoteCertificateData isEqualToData:localCertificate] && certificateIsValid) {
        NSURLCredential *credential = [NSURLCredential credentialForTrust:serverTrust];
        completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
    }
    else {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, NULL);
    }
}

#pragma mark - GET Request

- (void)performGETRequestWithURL:(NSString *)URLString requestHeaders:(NSDictionary *)headersDict requestParameters:(NSDictionary *)parametersDict andCompletionBlock:(DataDownloadBlock)completionBlock {
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config
                                                          delegate:self
                                                     delegateQueue:nil];
    
    NSString *params = [self buildQueryStringFromDictionary:parametersDict];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", URLString, params]]];
    request.HTTPMethod = @"GET";
    

    if (![headersDict count]) {
        
        //Creating ID for session
        if (!self.sessionID) {
            self.sessionID = [self generateUUID];
        }
        
        // adding other default parameters
        [request addValue: self.sessionID forHTTPHeaderField:@"Process-ID"];
        [request addValue: [self generateUUID] forHTTPHeaderField:@"Request-ID"];
        [request addValue: [self getLocalTimeInRFC1123] forHTTPHeaderField:@"Date"];
    }
    
    NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*) response;
            if (httpResp.statusCode == 200) {
                
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:kNilOptions
                                                                           error:&error];
                
                BLOCK_SAFE_PERFORM(completionBlock, jsonData);
                
            }
        }
        
    }];
    
    [downloadTask resume];
}

@end
