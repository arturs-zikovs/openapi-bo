//
//  BOANetworkServiceDefines.h
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#ifndef openAPI_BOANetworkServiceDefines_h
#define openAPI_BOANetworkServiceDefines_h

@import Foundation;

#define kEndpointAccountsURL        @"https://gate.blueorangebank.com/open-api/accounts/"
#define kEndpointAccountByIDURL     @"https://gate.blueorangebank.com/open-api/accounts/%@/"
#define kEndpointTransactionsURL    @"https://gate.blueorangebank.com/open-api/accounts/%@/transactions/"

#define BLOCK_SAFE_PERFORM(block, ...) block ? block(__VA_ARGS__) : nil

typedef void (^DataDownloadBlock)(NSDictionary *data);



#endif
