//
//  BOANetworkService+Helpers.m
//  openAPI
//
//  Created by Artur on 13/08/2018.
//  Copyright © 2018 blueOrange. All rights reserved.
//

#import "BOANetworkService.h"

@implementation BOANetworkService (Helpers)

#pragma mark - Support methods

- (NSString *)generateUUID {
    return [[NSUUID UUID] UUIDString];
}

- (NSString *)getLocalTimeInRFC1123 {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    dateFormatter.dateFormat = @"EEE',' dd MMM yyyy HH':'mm':'ss 'GMT'";
    
    return [dateFormatter stringFromDate:[NSDate date]];
}

- (NSString *)convertDateToISODate:(NSDate *)dateToConvert {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    return [dateFormatter stringFromDate:dateToConvert];
}

- (NSString *)buildQueryStringFromDictionary:(NSDictionary *)parameters {
    NSString *urlVars = nil;
    for (NSString *key in parameters) {
        NSString *value = parameters[key];
        value = [value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
        urlVars = [NSString stringWithFormat:@"%@%@=%@", urlVars ? @"&": @"", key, value];
    }
    return [NSString stringWithFormat:@"%@%@", urlVars ? @"?" : @"", urlVars ? urlVars : @""];
}
@end
